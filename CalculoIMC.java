package provaConsiste;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class CalculoIMC {
	
public static void main(String[] args) throws IOException {
		
	File arquivoCSV = new File("E:\\dataset.csv");
		
	try {
		String linhasDoArquivo = new String();
			
		Scanner leitor = new Scanner(arquivoCSV);
				
		leitor.nextLine();
		
		FileWriter arq = new FileWriter("E:\\jardelPereiraDeCarvalhoJunior.txt");
		PrintWriter gravarArq = new PrintWriter(arq);
				
		while(leitor.hasNext()) {
				
			linhasDoArquivo = leitor.nextLine();
			String[] valoresEntreVirgulas = linhasDoArquivo.split(";");
					
			if(valoresEntreVirgulas.length > 2) {
						
				String nomes = valoresEntreVirgulas[0];
				String sobrenomes = valoresEntreVirgulas[1];
				float peso = Float.parseFloat(valoresEntreVirgulas[2].
						replace(",","."));
				float altura = Float.parseFloat(
						valoresEntreVirgulas[3].replace(",", "."));
					
				float IMC = (float) (peso / Math.pow(altura, 2));
							
				gravarArq.printf("%s %s %.2f %n",
					nomes.toUpperCase().replace(" ",""),
					sobrenomes.toUpperCase().replace("  ", " "), IMC);
				System.out.printf("%s %s %.2f %n",
						nomes.toUpperCase().replace(" ",""),
						sobrenomes.toUpperCase().replace("  ", " "), IMC);
			}
		}
		arq.close();
		leitor.close();
	}catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
}

